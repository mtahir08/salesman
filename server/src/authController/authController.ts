/// <reference path='../../../typings/tsd.d.ts' />
var fb = require("firebase");
module.exports = function (app, Q) {
    // var ref = new fb("https://salewhat.firebaseio.com");
    // var companyRef = new fb("https://salewhat.firebaseio.com/company");
    fb.initializeApp({
        serviceAccount: "./salewhat-bc6e11cf7788.json",
        databaseURL: "https://salewhat-55e66.firebaseio.com/"
    });
    var Ref = fb.database().ref();
    var usersRef = fb.database().ref("user");
    var companyRef = fb.database().ref("company");
    app.api.signup = function (req, res) {
        console.log("in signup");
        if (!req.body.isAdmin && req.body.isAdmin !== false) {
            req.body.isAdmin = true;
            delete req.body.re_pass;
            savingData(req.body, null)
                .then((data)=> {
                    res.send(data);
                }, (error)=> {
                    res.send(error);
                })
        } else {
            usersRef.child(req.body.ownerId).once("value", function (snapshot) {
                console.log("snapshot.val()", snapshot.val());
                app.db.funct.findUser(snapshot.val().userId, Q)
                    .then((user) => {
                        req.body.companyKey = req.body.companyId;
                        req.body.companyId = user.companyId;
                        savingData(req.body, user)
                            .then((data)=> {
                                res.send(data);
                            }, (error)=> {
                                res.send(error);
                            })
                    }, (userErr) => {
                        console.log(userErr);
                    })

            });
        }


    };
    function savingData(body, user) {
        var deferred = Q.defer();
        var userRef = usersRef.child(body.response.uid.toString());
        userRef.set({
            name: body.name,
            email: body.email,
            username: body.username,
            isAdmin: body.isAdmin,
            createdAt: fb.database.ServerValue.TIMESTAMP,
            updatedAt: fb.database.ServerValue.TIMESTAMP
        }, (err, data)=> {
            if (err) {
                console.log("err", err);
            }
            else {
                app.db.funct.signUp(body, body.response, Q)
                    .then(function (singupData) {
                        if (body.isAdmin == true) {
                            adminFunction(body, userRef, companyRef, singupData)
                                .then(function (adminFunctionSuccessReturn) {
                                    deferred.resolve({status: true, data: singupData});
                                }, function (adminFunctionErrorReturn) {
                                    deferred.reject({status: false, error: adminFunctionErrorReturn});
                                });
                        } else {
                            userRef.update({
                                companyId: body.companyId,
                                ownerId: body.ownerId
                            }, (err)=> {
                                if (err) {
                                    console.log("Err", err);
                                } else {
                                    companyRef.child(body.companyKey).child("salesmen").child(body.response.uid).update({
                                        name: body.name,
                                        email: body.email,
                                        username: body.username,
                                        isAdmin: body.isAdmin,
                                        createdAt: fb.database.ServerValue.TIMESTAMP,
                                        updatedAt: fb.database.ServerValue.TIMESTAMP
                                    }, (error) => {
                                        if (error) {
                                            console.log("error", error);
                                        } else {
                                            deferred.resolve({
                                                status: true,
                                                message: 'successfully signup',
                                                data: data
                                            });
                                        }
                                    });
                                }
                            });

                        }
                    }, (err)=> {
                        console.log("authentication unsuccessful", err);
                        deferred.reject({status: false, message: 'authentication unsuccessful', error: err});
                    });
            }
        });
        return deferred.promise;
    }

    function adminFunction(body, userRef, companyRef, data) {
        var deferred = Q.defer();
        var companyKey = companyRef.push();
        companyKey = companyKey.toString().split('/').pop();
        companyRef.child(companyKey).child(body.company).set({
            name: body.company,
            ownerId: body.response.uid,
            createdAt: fb.database.ServerValue.TIMESTAMP,
            updatedAt: fb.database.ServerValue.TIMESTAMP
        }, function (error) {
            if (error) {
                console.log("firebase error in creatig company: 70", error);
                deferred.reject({status: false, message: 'Error creating company', error: error});
            } else {
                userRef.update({
                    company: body.company,
                    companyId: companyKey,
                    userId: data._id
                });
                app.db.funct.company(data.name, data._id, Q)
                    .then((companyData)=> {
                        console.log("Successfully created Company in mongodb:144");
                        app.db.funct.updateUser(data._id, companyData._id, Q)
                            .then((updated)=> {
                                deferred.resolve(userRef);
                            }, (updatedErr)=> {
                                console.log("User updated Err", updatedErr);
                                deferred.reject(updatedErr);
                            })
                    }, (companyErr)=> {
                        console.log("company can not created : 147", companyErr);
                        deferred.reject({
                            status: false,
                            message: 'Error creating company',
                            error: companyErr
                        });
                    })
            }
        });
        return deferred.promise;
    }

    app.api.login = function (req, res) {
        console.log(req.body.email);
        if (req.body.authData.password && req.body.authData.password.isTemporaryPassword === true) {
            res.send({
                status: true, message: 'Temporary Password', data: {
                    uid: req.body.authData.auth.uid,
                    email: req.body.email,
                    tempPass: req.body.password,
                    tempPassTrue: req.body.authData.password.isTemporaryPassword,
                    path: '/resetPassword'
                }
            })
        }
        else {
            var obj = {
                user: undefined,
                authData: undefined
            };
            // if(req.body.isAdmin === false){

            usersRef.child(req.body.authData.uid).update({
                login: true,
                updatedAt: fb.database.ServerValue.TIMESTAMP
            }, function (err, data) {
                usersRef.child(req.body.authData.uid).once('value', function (snap) {
                    obj.user = snap.val();
                    delete obj.user['isAdmin'];
                }).then(function () {
                    console.log("Authenticated successfully with payload:", req.body.authData);
                    res.send({status: true, message: 'Login successfull!!', obj: obj, authData: req.body.authData})
                });

            });

        }
    };
    /*     app.api.forgotPass = function(req, res){
     console.log(req.body);
     ref.resetPassword({
     email: req.body.email
     }, (error) => {
     if (error === null) {
     console.log("Password reset email sent successfully");
     res.send({status: false, message: 'Password reset email sent successfully'})
     } else {
     console.log("Error sending password reset email:", error);
     res.send({status: false, message: 'Error sending password reset email', error : error});

     }
     });

     }
     app.api.resetPass = function(req, res){
     console.log(req.body);
     ref.changePassword({
     email: req.body.email,
     oldPassword: req.body.oldPass,
     newPassword: req.body.password
     }, function (error) {
     if (error === null) {
     console.log("Password changed successfully");
     res.send({status: true, message: 'Password changed successfully', path: "/dashboard"});
     // res.json({path: "/dashboard"});
     } else {
     console.log("Error changing password:", error);
     // res.json({Error: error});
     res.send({status: false, message: 'Password change unsuccessfully', Error: error});
     }
     });

     }*/
    app.api.logout = function (req, res) {
        usersRef.child(req.body.uid).update({
            login: false
        }, (error, authData)=> {
            if (error) {
                res.send({status: false, message: 'logout unsuccessful', error: error});
                // res.json({'error': error})
            }
            else {
                // res.json(authData)
                res.send({status: true, message: 'logout successfully'});
            }
        });
    }
};