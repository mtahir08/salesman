/// <reference path='../../../typings/tsd.d.ts' />
module.exports = function (app, mongoose) {
    require('./config')(app, mongoose);
    require('./firebase')(app);
};